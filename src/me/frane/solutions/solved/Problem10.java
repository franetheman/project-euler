package me.frane.solutions.solved;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem10
{

    public static long sumOfPrimes()
    {
        long sum = 2;

        for(int i = 3 ; i < 2000000 ; i += 2)
        {
            if(isPrime(i))
                sum += i;
        }

        return sum;
    }


    public static boolean isPrime(int n)
    {
        if(n == 2)
            return true;

        if (n % 2 == 0)
            return false;

        for(int i = 3 ; i <= Math.pow(n, 0.5) ; i += 2)
        {
            if (n % i == 0)
                return false;
        }

        return true;
    }

}
