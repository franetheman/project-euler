package me.frane.solutions.solved;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem3
{
    public static ArrayList<BigInteger> primeBigFactorize(BigInteger n)
    {
        ArrayList<BigInteger> primeFactors = new ArrayList<BigInteger>();
        BigInteger primeFactor = BigInteger.ZERO;

        for (BigInteger i = new BigInteger("2"); i.compareTo(n.divide(i)) <= 0; )
        {
            if (n.mod(i).longValue() == 0)
            {
                primeFactor = i;
                primeFactors.add(primeFactor);
                n = n.divide(i);
            } else
                {
                i = i.add(BigInteger.ONE);
            }
        }

        if (primeFactor.compareTo(n) < 0)
        {
            primeFactors.add(n);
        }
        else
        {
            primeFactors.add(primeFactor);
        }

        return primeFactors;
    }
}
