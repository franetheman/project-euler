package me.frane.solutions.solved;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Frane on 06-01-2018.
 */
public class Problem23
{
    public static int nonAbundantSum()
    {
        int sum = 0;
        Set<Integer> abundant = new HashSet<>();
        Set<Integer> abundantSums = new HashSet<>();

        for(int i = 0; i < 21823; i++)
            if(isAbundant(i))
                abundant.add(i);

        for(int i : abundant)
            for(int j : abundant)
                abundantSums.add(i + j);

        for(int i = 0; i < 21823; i++)
            if(!abundantSums.contains(i))
                sum += i;

        return sum;
    }

    public static boolean isAbundant(int num)
    {
        if(num < 12)
            return false;

        return sumDivisors(num) > num;
    }

    private static int sumDivisors(int num)
    {
        int divisors = 1;

        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0)
                divisors += i;
        }

        return divisors;
    }
}
