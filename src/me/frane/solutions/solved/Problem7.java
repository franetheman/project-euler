package me.frane.solutions.solved;

import java.util.stream.IntStream;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem7
{

    public static int get10001stPrime()
    {
        int primeAmount = 0;

        int currentNum = 0;

        while(primeAmount <= 10000)
        {
            if(isPrime(currentNum))
                primeAmount++;

            currentNum++;
        }

        return currentNum-1;
    }

    private static boolean isPrime(int number)
    {
        return number >= 2
                && IntStream.rangeClosed(2, (int) Math.sqrt(number))
                .noneMatch(n -> (number % n == 0));
    }
}
