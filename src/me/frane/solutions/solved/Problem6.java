package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem6
{
    public static int sumSquareDifference()
    {
        int sumSquares = 0;
        int squareSum = 0;

        for(int i = 1 ; i <= 100 ; i++)
        {
            sumSquares += Math.pow(i, 2);
        }

        for(int i = 1 ; i <= 100 ; i++)
        {
            squareSum += i;
        }

        squareSum = (int) Math.pow(squareSum, 2);

        return squareSum - sumSquares;
    }
}
