package me.frane.solutions.solved;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.util.Arrays.stream;

/**
 * Created by Frane on 02-10-2017.
 */
public class Problem18And67
{
    public static int maximumPathSum() throws IOException
    {
        int[][] data = Files.lines(Paths.get("C:/Users/Frane/Desktop/triangle.txt"))
                .map(s -> stream(s.trim().split("\\s+"))
                        .mapToInt(Integer::parseInt)
                        .toArray())
                .toArray(int[][]::new);

        for (int r = data.length - 1; r > 0; r--)
            for (int c = 0; c < data[r].length - 1; c++)
                data[r - 1][c] += Math.max(data[r][c], data[r][c + 1]);

        return data[0][0];
    }
}
