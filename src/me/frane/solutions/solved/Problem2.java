package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem2
{
    public static int evenFibonaci()
    {
        int maxValue = 4000000;

        int sum = 0;

        int first = 1;
        int second = 1;

        int result = 0;

        while(result < maxValue)
        {
            if(result % 2 == 0)
                sum += result;

            first = second;
            second = result;

            result = first + second;
        }

        return sum;
    }
}
