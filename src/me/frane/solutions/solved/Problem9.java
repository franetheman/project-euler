package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem9
{
    public static int specialPythagoeranTriplet()
    {
        int sum = 1000;

        for(int a = 1 ; a < sum ; a++)
        {
            for(int b = 1 ; b < sum ; b++)
            {
                int c = sum - a - b;

                if(a * a + b * b == c * c)
                    return a * b * c;
            }
        }

        return 0;
    }
}
