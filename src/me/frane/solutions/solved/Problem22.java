package me.frane.solutions.solved;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by Frane on 06-01-2018.
 */
public class Problem22
{

    public static BigInteger nameScores(File file)
    {
        BigInteger result = new BigInteger("0");
        final int offset = 64;

        String[] names = processFile(file);

        for(int i = 0; i < names.length; i++)
        {
            names[i] = names[i].substring(1, names[i].length() -1);

            int pos = i;
            int namescore = 0;

            for(char c : names[i].toCharArray())
            {
                namescore += (int)c - offset;
            }

            namescore = namescore * (pos + 1);

            result = result.add(new BigInteger(String.valueOf(namescore)));
        }

        return result;
    }

    private static String[] processFile(File file)
    {
        try
        {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String nameString = bufferedReader.readLine();

            reader.close();
            bufferedReader.close();

            String[] splitNames = nameString.split(",");

            Arrays.sort(splitNames);

            return splitNames;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

}
