package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem5
{
    public static int smallestMultiple()
    {
        for(int i = 1 ; ; i++)
        {
            if(isDivisible(i))
                return i;
        }
    }

    private static boolean isDivisible(int i)
    {
        for(int j = 1 ; j <= 20 ; j++)
        {
            if(i%j != 0)
                return false;
        }

        return true;
    }
}
