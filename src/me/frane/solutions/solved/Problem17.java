package me.frane.solutions.solved;

/**
 * Created by Frane on 02-10-2017.
 */
public class Problem17
{
    private static String[] ones = {"","one","two","three","four","five","six","seven","eight","nine","ten",
            "eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};

    private static String[] tens = {"","ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};

    private static String[] hundreds = {"", "onehundred","twohundred","threehundred","fourhundred","fivehundred","sixhundred",
            "sevenhundred","eighthundred","ninehundred"};

    public static int numberLettersCount()
    {
        int count = 0;

        for(int i = 1 ; i <= 1000 ; i++)
        {
            System.out.println(word(i));
            count += word(i).length();
        }

        return count;
    }

    private static String word(int num)
    {
        int temp = num;
        String word = "";

        if(temp == 1000)
            return "onethousand";

        if(temp >= 100)
        {
            word += hundreds[temp/100];
            temp = temp % 100;

            if(temp % 100 != 0)
                word += "and";
        }

        if(temp >= 20)
        {
            word += tens[temp/10];
            temp = temp % 10;
        }

        if(temp < 20)
        {
            word += ones[temp];
        }

        return word;
    }

}
