package me.frane.solutions.solved;

/**
 * Created by Frane on 06-01-2018.
 */
public class Problem21
{
    public static int sumOfAmicableNumbers()
    {
        int sum = 0;

        for(int i = 0; i < 10000; ++i)
        {
            if(isAmicableNumber(i))
                sum += i;
        }

        return sum;
    }

    public static boolean isAmicableNumber(int num)
    {
        int firstSumOfProperDivisors = 0;

        for(int i = 1; i < num; i++)
        {
            if(num % i == 0)
                firstSumOfProperDivisors += i;
        }

        int secondSumOfProperDivisors = 0;

        for(int i = 1; i < firstSumOfProperDivisors; i++)
        {
            if(firstSumOfProperDivisors % i == 0)
                secondSumOfProperDivisors += i;
        }

        if(secondSumOfProperDivisors == num && num != firstSumOfProperDivisors)
            System.out.println(num);

        return secondSumOfProperDivisors == num && num != firstSumOfProperDivisors;
    }
}
