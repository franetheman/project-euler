package me.frane.solutions.solved;

/**
 * Created by Frane on 06-01-2018.
 */
public class Problem19
{
    public static int countSundays()
    {
        int sundays = 0;

        for(int y = 1901; y < 2000; y++)
        {
            for(int m = 1; m <= 12; m++)
            {
                if(dayOfWeek(1, m, y) == 0)
                    sundays++;
            }
        }

        return sundays;
    }

    public static int dayOfWeek(int day, int month, int year) // Remember this shit in the future lol
    {
        int c, y, m, d;
        int cc, yy;

        cc = year / 100;
        yy = year - ((year/100)*100);

        c = (cc/4) - 2*cc-1;
        y = 5*yy/4;
        m = 26*(month+1)/10;
        d = day;

        return (c+y+m+d)%7;
    }

    private static boolean isLeapYear(int year)
    {
        assert year >= 1583; // not valid before this date.
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }
}
