package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem4
{
    public static int largestPolindrome()
    {
        int largest = 0;
        int jj = 0;
        int ii = 0;

        for(int i = 100 ; i <= 1000 ; i++)
        {
            for(int j = 100 ; j <= 1000 ; j++)
            {
                if(isPolindrome(i*j) && largest < i*j)
                {
                    largest = i*j;
                    jj = j;
                    ii = i;
                }
            }
        }

        System.out.println(jj);
        System.out.println(ii);

        return largest;
    }

    private static boolean isPolindrome(int number)
    {
        String s = Integer.toString(number);
        String reversed = new StringBuilder(s).reverse().toString();

        return s.equals(reversed);
    }
}
