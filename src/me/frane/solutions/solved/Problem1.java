package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem1
{
    public static int multiplesOf3And5(int below)
    {
        int sum = 0;

        for(int i = 0 ; i < below ; i++)
        {
            if(i%3 == 0 || i%5 == 0)
            {
                sum += i;
            }
        }

        return sum;
    }
}
