package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem8
{
    public static long largestProductInSeries(String series)
    {
        long one = 0, two = 0, three = 0, four = 0, five = 0,
                six = 0, seven = 0, eight = 0, nine = 0, ten = 0, eleven = 0, twelve = 0, thirteen = 0;

        long largestProduct = 0;

        char[] numbers = series.toCharArray();


        for(int i = 0 ; i < numbers.length - 12 ; i++)
        {
            one = Long.parseLong(String.valueOf(numbers[i]));
            two = Long.parseLong(String.valueOf(numbers[i+1]));
            three = Long.parseLong(String.valueOf(numbers[i+2]));
            four = Long.parseLong(String.valueOf(numbers[i+3]));
            five = Long.parseLong(String.valueOf(numbers[i+4]));
            six = Long.parseLong(String.valueOf(numbers[i+5]));
            seven = Long.parseLong(String.valueOf(numbers[i+6]));
            eight = Long.parseLong(String.valueOf(numbers[i+7]));
            nine = Long.parseLong(String.valueOf(numbers[i+8]));
            ten = Long.parseLong(String.valueOf(numbers[i+9]));
            eleven = Long.parseLong(String.valueOf(numbers[i+10]));
            twelve = Long.parseLong(String.valueOf(numbers[i+11]));
            thirteen = Long.parseLong(String.valueOf(numbers[i+12]));

            long product = one * two * three * four * five * six * seven * eight * nine * ten * eleven * twelve * thirteen;

            if(largestProduct < product)
                largestProduct = product;
        }

        return largestProduct;
    }
}
