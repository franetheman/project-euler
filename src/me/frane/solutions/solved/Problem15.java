package me.frane.solutions.solved;

/**
 * Created by Frane on 02-10-2017.
 */
public class Problem15
{
    public static long latticePath()
    {
        int x = 20, y = 20;

        return binomial(x+y, x);
    }

    private static long binomial(int n, int k)
    {
        if (k>n-k)
            k=n-k;

        long b=1;
        for (int i=1, m=n; i<=k; i++, m--)
            b=b*m/i;
        return b;
    }
}
