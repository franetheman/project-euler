package me.frane.solutions.solved;

/**
 * Created by Frane on 02-10-2017.
 */
public class Problem14
{

    public static int longestCollatz()
    {
        int longestCollatzValue = 0;
        long mostSteps = 0;

        for(int i = 1 ; i <= 1000000 ; i++)
        {
            if(i % 1000 == 0)
                System.out.println(i);

            long steps = collatz(i);
            if(steps > mostSteps)
            {
                longestCollatzValue = i;
                mostSteps = steps;
            }
        }

        return longestCollatzValue;
    }

    private static long collatz(int num)
    {
        long temp = num;
        long steps = 0;

        while(temp != 1)
        {
            if(temp % 2 == 0)
                temp /= 2;

            else
                temp = 3 * temp + 1;

            steps += 1;
        }

        return steps;
    }
}
