package me.frane.solutions.solved;

import java.math.BigInteger;

/**
 * Created by Frane on 06-01-2018.
 */
public class Problem20
{
    public static BigInteger factorial(int n)
    {
        BigInteger temp = new BigInteger("1");

        BigInteger result = new BigInteger("0");

        for(int i = n ; i > 0 ; i--)
        {
            temp = temp.multiply(new BigInteger(String.valueOf(i)));
        }

        String[] tokens = temp.toString().split("");

        for(String s : tokens)
        {
            result = result.add(new BigInteger(s));
        }

        return result;
    }
}
