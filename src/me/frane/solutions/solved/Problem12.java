package me.frane.solutions.solved;

/**
 * Created by Frane on 01-10-2017.
 */
public class Problem12
{
    public static long highlyDivisibleTrigularNumber()
    {
        int currentNum = 1;

        while(true)
        {
            long triangleNumberValue = 0;

            for(int i = 0 ; i <= currentNum ; i++)
            {
                triangleNumberValue += i;
            }

            System.out.println(triangleNumberValue);

            if(getDivisorsForNum(triangleNumberValue) >= 500)
                return triangleNumberValue;

            currentNum++;
        }
    }

    private static int getDivisorsForNum(long number)
    {
        int nod = 0;
        int sqrt = (int) Math.sqrt(number);

        for(int i = 1; i<= sqrt; i++)
        {
            if(number % i == 0)
            {
                nod += 2;
            }
        }
        //Correction if the number is a perfect square
        if (sqrt * sqrt == number)
        {
            nod--;
        }

        return nod;
    }
}
