package me.frane.solutions.solved;

import java.math.BigInteger;

/**
 * Created by Frane on 02-10-2017.
 */
public class Problem16
{
    public static long powerDigitSum()
    {
        long sum = 0;

        BigInteger integer = new BigInteger("2");

        integer = integer.pow(1000);

        char[] numbers = integer.toString().toCharArray();

        for(char c : numbers)
        {
            sum += Long.parseLong(String.valueOf(c));
        }

        return sum;
    }
}
